## 參考
- Ref: https://stackoverflow.com/questions/51439912/deploy-aiohttp-on-heroku
- git，
  - https://git.heroku.com/ching-finance-server.git
  - https://gitlab.com/anthony076/finance-inspector
- access url，
  - https://ching-finance-server.herokuapp.com/

## twse-api
- 取得當月所有交易日資料
  https://www.twse.com.tw/exchangeReport/STOCK_DAY?response=json&date=20200901&stockNo=2303
  1. 返回的資料會自動略過非交易日，

  2. 日期只要在當月的時間內，都會顯示當月的資料，
     若日期超過當月的時間，系統會自動將查詢日期改成正確的時間後，返回正確時間當月的日期

     例如，9月只有30天，
     date=20200930，返回9月的資料
     date=20200931，系統將查詢時間變更為 date=20201001，返回10月的資料

- 單筆的即時資料
  http://mis.twse.com.tw/stock/api/getStockInfo.jsp?ex_ch=tse_2330.tw&_=1605872615000

- 多筆的即時資料
  http://mis.twse.com.tw/stock/api/getStockInfo.jsp?ex_ch=tse_2330.tw|tse_2603.tw&_=1605872801000

  返回
  "tv": "62",             trade_volume
  "ps": "62",
  "pz": "32.7000",
  "fv": "16",
  "oa": "0.0000",
  "ob": "32.7000",
  "a": "-",                   best_ask_price
  "b": "32.7000_32.6500_32.6000_32.5500_32.5000_",            best_bid_price
  "c": "2338",            code 股票代碼
  "d": "20201116",
  "ch": "2338.tw",
  "ot": "14:30:00",
  "tlong": "1605508200000",
  "f": "-",                   best_ask_volume
  "ip": "0",
  "g": "2281_95_56_37_73_",               best_bid_volume
  "mt": "000000",
  "ov": "12434",
  "h": "32.7000",             high，當日最高價
  "i": "24",
  "it": "12",
  "oz": "32.7000",
  "l": "30.4000",             low，當日最低價
  "n": "光罩",
  "o": "30.4000",             open，開盤價
  "p": "0",
  "ex": "tse",
  "s": "62",
  "t": "13:30:00",
  "u": "32.7000",
  "v": "49662",           accumulate_trade_volume
  "w": "26.8000",
  "nf": "台灣光罩股份有限公司", fullname 完整名稱
  "y": "29.7500",
  "z": "32.7000",
  "ts": "0"           latest_trade_price

  
- 限制，
  
  - 5秒內不能超過 3個 req
  
  - 不掛代理的情況下，最多可同時查詢 4 支股票
  
  - 等待 proxy-pool 的數量 大於股票數的 2 倍後才進行資料撈取
  
    
  
- 對 twse 限制的思路
  - 實現兩種方式存取 twse，
    - proxy模式，當爬取的 proxy 數量到達預期值 / 2 的時候使用
    
    - proxy模式，直到爬取到足夠的 proxy 數量後才啟動後續動作
    
    - 慢速模式，將 realtime-stock 和 month-stock 分離，month-stock 每次只更新兩個
    
      

## Proxy-Pool 相關

- 更新機制

  - step1，get raw-proxies from proxy file(old and valided proxy) and web(new/old-raw-proxy) 

    ​	> removeDuplicated > verify raw-proxies

  - step2，verify raw-proxies in step1, then product latest-validList (means latest-valid-proxies)

  - step3，assign latest-validList to self.__proxyList, and write into proxy-file, 	

    > caution，you dont need failList in this step. because all of proxy in latest-validList are just valided in step2    

  - step4，wait 60s

  - step5，repeat step1 ~ step4 again

  - 缺點，無法保證 proxy-pool 維持在一定的數量

    - 解決方式1，尋找更多的 proxy-source
    
    - 解決方式2，web-server 啟動前，確保 proxy-file 中有足夠多的 proxy
    
      

## Procifile

- 若要使用 gunicorn，需要添加依賴 gunicorn 的依賴，會安裝  gunicorn-cli
  並透過  gunicorn-cli 執行 aiohttp-server
  命令見 Procfile 檔



