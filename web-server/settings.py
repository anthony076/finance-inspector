import aiohttp

class Pool:
    # proxy-pool 更新的週期 (單位: 秒)
    interval:int = 30

    # 測試 proxy 的可用性時，等待 proxy 回應的時間 (單位: 秒)
    timeout:int = 20

class Twse:
    useProxy:bool = True

class Twstk:
    useProxy:bool = True

    # 台康生技 6589, 億光 2393, 巧新 1563, 加權指, 台積電 2330, 東科-ky 5225, 
    # 漢唐 2404, 玉山金 2884, 街口布蘭特油正2 00715L, 嘉泥 1103, 臺企銀 2834, 晶電 
    
    # work 2303 2330 2393 5225 2884 00715L 1103 2834 2448
    # not work 6589 1563
    targetStocks:list = ["2303", "2330", "2393", "5225", "2884", "00715L", "1103", "2834", "2448"]


class Setting:
    # 背景任務執行更新的間格時間 (單位: 秒)
    crawler_interval = 60

    # server 初啟動後，需要等待抓取第一批 proxy 的時間
    wait_first_proxies = 30

    # 向 twse 發出 req 後，等待 twse 回應的時間 (單位: 秒)
    req_timeout = aiohttp.ClientTimeout(total=120)

    pool = Pool()
    twse = Twse()
    twstk = Twstk()

setting = Setting()