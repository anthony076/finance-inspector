import asyncio
import datetime
import json

from aiohttp import web
from settings import setting

from handlers.stockq import Stockq
from handlers.twse import Twse
from handlers.twstk import Twstk

from utils.util import updatAllStock
from utils.mylogger import mainlogger
from utils.proxypool import ProxyPool
pp = ProxyPool()

latest_result = {
    "stockq": {},
    "twse": {},
    "twstk": {}
}

# ======================
# 定時任務
# ======================
async def crawler(proxypool: ProxyPool, interval: int = 60):
    global latest_result

    # wait proxypool to get fist batch proxies
    await asyncio.sleep(setting.wait_first_proxies)

    stockq = Stockq()
    twse = Twse(proxypool=proxypool)
    twstk = Twstk(proxypool=proxypool)

    handlerList = [stockq, twse, twstk]

    while True:
        mainlogger.info(
            f"[BG]: update stock infomation @ {datetime.datetime.now()}")

        tasks = [asyncio.create_task(handler.run()) for handler in handlerList]

        try:
            await asyncio.gather(*tasks, return_exceptions=True)

            latest_result["stockq"] = stockq.data
            latest_result["twse"] = twse.data
            latest_result["twstk"] = updatAllStock(
                latest_result["twstk"], twstk.data)
            latest_result["lastest-update"] = str(datetime.datetime.now())

        except Exception as e:
            mainlogger.error("==== Error in crawler ====")
            mainlogger.error(f"{e}")

        await asyncio.sleep(interval)

# ======================
# 註冊背景定時任務
# ======================
async def reg_bg_tasks(app):
    app["proxy-pool"] = asyncio.create_task(pp.updatePool4ever(
        interval=setting.pool.interval, timeout=setting.pool.timeout))
    app["bg-task"] = asyncio.create_task(
        crawler(proxypool=pp, interval=setting.crawler_interval))

# ======================
# web-api
# ======================
class WebHandler:
    def __init__(self):
        pass

    async def root(self, request):
        global latest_result

        mainlogger.info(
            f"[Web] user request comming @ {datetime.datetime.now()}...")

        return web.Response(text=json.dumps(latest_result, ensure_ascii=False))

    async def stockq(self, request):
        global latest_result

        return web.Response(text=json.dumps(latest_result["stockq"], ensure_ascii=False))

    async def twse(self, request):
        global latest_result

        return web.Response(text=json.dumps(latest_result["twse"], ensure_ascii=False))

    async def twstk(self, request):
        global latest_result

        return web.Response(text=json.dumps(latest_result["twstk"], ensure_ascii=False))

    async def proxy(self, request):
        global pp

        return web.Response(text="\n".join(str(item) for item in pp.proxyList))


async def create_app():
    app = web.Application()

    # 添加路由
    webhandler = WebHandler()
    app.add_routes([
        web.get('/', webhandler.root),
        web.get('/stockq', webhandler.stockq),
        web.get('/twse', webhandler.twse),
        web.get('/twstk', webhandler.twstk),
        web.get("/proxy", webhandler.proxy),
    ])

    # 添加背景任務
    app.on_startup.append(reg_bg_tasks)

    return app


def main():
    app = create_app()
    web.run_app(app, host="127.0.0.1", port=5566)


if __name__ == "__main__":
    main()
