
class Url:
    stockq = "http://www.stockq.org/"
    # https://mis.twse.com.tw/stock/etf_nav.jsp?ex=tse
    twse = "https://mis.twse.com.tw/stock/data/all_etf.txt"

class Stockq:
    tables = 'table.marketdatatable'
    catalogName = 'tr:nth-child(1)'
    values = 'tr.row1 td[nowrap="nowrap"], tr.row2 td[nowrap="nowrap"]'

class CSS:
    stockq = Stockq()
    url = Url()

css = CSS()