from aiohttp import ClientSession

from settings import setting
from utils.proxypool import ProxyPool
from utils.mylogger import utillogger

catalogMap = {
    "1":"【國內成分證券ETF】-新台幣交易",
    "2":"【標的指數或商品位於亞洲時區之ETF】-新台幣交易",
    "3":"【標的指數或商品位於亞洲時區之ETF】-外幣交易",
    "4":"【標的指數或商品位於歐美時區之ETF】-新台幣交易",
    "5":"【標的指數或商品位於歐美時區之ETF】-外幣交易",
    "6":"【全球時區ETF】-新台幣交易",
    "7":"Unknown"
}

nameMap = {
    "0050": {"name":"元大台灣50","catalog":"1"},
    "0051": {"name":"元大中型100","catalog":"1"},
    "0052": {"name":"富邦科技","catalog":"1"},
    "0053": {"name":"元大電子","catalog":"1"},
    "0054": {"name":"元大台商50","catalog":"1"},
    "0055": {"name":"元大MSCI金融","catalog":"1"},
    "0056": {"name":"元大高股息","catalog":"1"},
    "0057": {"name":"富邦摩台","catalog":"1"},
    "0061": {"name":"元大寶滬深","catalog":"2"},
    "006203": {"name":"元大MSCI台灣","catalog":"1"},
    "006204": {"name":"永豐臺灣加權","catalog":"1"},
    "006205": {"name":"富邦上証","catalog":"2"},
    "006206": {"name":"元大上證50","catalog":"2"},
    "006207": {"name":"FH滬深","catalog":"2"},
    "006208": {"name":"富邦台50","catalog":"1"},
    "00625K": {"name":"富邦上証+R","catalog":"3"},
    "00631L": {"name":"元大台灣50正2","catalog":"1"},
    "00632R": {"name":"元大台灣50反1","catalog":"1"},
    "00633L": {"name":"富邦上証正2","catalog":"2"},
    "00634R": {"name":"富邦上証反1","catalog":"2"},
    "00635U": {"name":"元大S&P黃金","catalog":"4"},
    "00636": {"name":"國泰中國A50","catalog":"2"},
    "00636K": {"name":"國泰中國A50+U","catalog":"3"},
    "00637L": {"name":"元大滬深300正2","catalog":"2"},
    "00638R": {"name":"元大滬深300反1","catalog":"2"},
    "00639": {"name":"富邦深100","catalog":"2"},
    "00640L": {"name":"富邦日本正2","catalog":"2"},
    "00641R": {"name":"富邦日本反1","catalog":"2"},
    "00642U": {"name":"元大S&P石油","catalog":"4"},
    "00643": {"name":"群益深証中小","catalog":"2"},
    "00643K": {"name":"群益深証中小+R","catalog":"3"},
    "00645": {"name":"富邦日本","catalog":"2"},
    "00646": {"name":"元大S&P500","catalog":"4"},
    "00647L": {"name":"元大S&P500正2","catalog":"4"},
    "00648R": {"name":"元大S&P500反1","catalog":"4"},
    "00650L": {"name":"FH香港正2","catalog":"2"},
    "00651R": {"name":"FH香港反1","catalog":"2"},
    "00652": {"name":"富邦印度","catalog":"2"},
    "00653L": {"name":"富邦印度正2","catalog":"2"},
    "00654R": {"name":"富邦印度反1","catalog":"2"},
    "00655L": {"name":"國泰中國A50正2","catalog":"2"},
    "00656R": {"name":"國泰中國A50反1","catalog":"2"},
    "00657": {"name":"國泰日經225","catalog":"2"},
    "00657K": {"name":"國泰日經225+U","catalog":"3"},
    "00660": {"name":"元大歐洲50","catalog":"4"},
    "00661": {"name":"元大日經225","catalog":"2"},
    "00662": {"name":"富邦NASDAQ","catalog":"4"},
    "00663L": {"name":"國泰臺灣加權正2","catalog":"1"},
    "00664R": {"name":"國泰臺灣加權反1","catalog":"1"},
    "00665L": {"name":"富邦恒生國企正2","catalog":"2"},
    "00666R": {"name":"富邦恒生國企反1","catalog":"2"},
    "00668": {"name":"國泰美國道瓊","catalog":"4"},
    "00668K": {"name":"國泰美國道瓊+U","catalog":"5"},
    "00669R": {"name":"國泰美國道瓊反1","catalog":"4"},
    "00670L": {"name":"富邦NASDAQ正2","catalog":"4"},
    "00671R": {"name":"富邦NASDAQ反1","catalog":"4"},
    "00673R": {"name":"元大S&P原油反1","catalog":"4"},
    "00674R": {"name":"元大S&P黃金反1","catalog":"4"},
    "00675L": {"name":"富邦臺灣加權正2","catalog":"1"},
    "00676R": {"name":"富邦臺灣加權反1","catalog":"1"},
    "00677U": {"name":"富邦VIX","catalog":"4"},
    "00678": {"name":"群益NBI生技","catalog":"4"},
    "00680L": {"name":"元大美債20正2","catalog":"4"},
    "00681R": {"name":"元大美債20反1","catalog":"4"},
    "00682U": {"name":"元大美元指數","catalog":"4"},
    "00683L": {"name":"元大美元指數正2","catalog":"4"},
    "00684R": {"name":"元大美元指數反1","catalog":"4"},
    "00685L": {"name":"群益臺灣加權正2","catalog":"1"},
    "00686R": {"name":"群益臺灣加權反1","catalog":"1"},
    "00688L": {"name":"國泰20年美債正2","catalog":"4"},
    "00689R": {"name":"國泰20年美債反1","catalog":"4"},
    "00690": {"name":"兆豐藍籌30","catalog":"1"},
    "00691R": {"name":"兆豐藍籌30反1","catalog":"1"},
    "00692": {"name":"富邦公司治理","catalog":"1"},
    "00693U": {"name":"街口S&P黃豆","catalog":"4"},
    "00700": {"name":"富邦恒生國企","catalog":"2"},
    "00701": {"name":"國泰股利精選30","catalog":"1"},
    "00702": {"name":"國泰標普低波高息","catalog":"4"},
    "00703": {"name":"台新MSCI中國","catalog":"4"},
    "00706L": {"name":"元大S&P日圓正2","catalog":"4"},
    "00707R": {"name":"元大S&P日圓反1","catalog":"4"},
    "00708L": {"name":"元大S&P黃金正2","catalog":"4"},
    "00709": {"name":"富邦歐洲","catalog":"4"},
    "00710B": {"name":"FH彭博高收益債","catalog":"4"},
    "00711B": {"name":"FH彭博新興債","catalog":"4"},
    "00712": {"name":"FH富時不動產","catalog":"4"},
    "00713": {"name":"元大台灣高息低波","catalog":"1"},
    "00714": {"name":"群益道瓊美國地產","catalog":"4"},
    "00715L": {"name":"街口布蘭特油正2","catalog":"4"},
    "00717": {"name":"富邦美國特別股","catalog":"4"},
    "00728": {"name":"第一金工業30","catalog":"1"},
    "00730": {"name":"富邦臺灣優質高息","catalog":"1"},
    "00731": {"name":"FH富時高息低波","catalog":"1"},
    "00732": {"name":"國泰RMB短期報酬","catalog":"2"},
    "00733": {"name":"富邦臺灣中小","catalog":"1"},
    "00735": {"name":"國泰臺韓科技","catalog":"2"},
    "00736": {"name":"國泰新興市場","catalog":"6"},
    "00737": {"name":"國泰AI+Robo","catalog":"6"},
    "00738U": {"name":"元大道瓊白銀","catalog":"4"},
    "00739": {"name":"元大MSCI A股","catalog":"2"},
    "00742": {"name":"新光內需收益","catalog":"1"},
    "00743": {"name":"國泰中國A150","catalog":"2"},
    "00752": {"name":"中信中國50","catalog":"2"},
    "00753L": {"name":"中信中國50正2","catalog":"2"},
    "00757": {"name":"統一FANG+","catalog":"4"},
    "00762": {"name":"元大全球AI","catalog":"6"},
    "00763U": {"name":"街口道瓊銅","catalog":"4"},
    "00766L": {"name":"台新MSCI中國正2","catalog":"6"},
    "00770": {"name":"國泰北美科技","catalog":"4"},
    "00771": {"name":"元大US高息特別股","catalog":"4"},
    "00774B": {"name":"新光中國政金綠債","catalog":"2"},
    "00774C": {"name":"新光中政金綠債+R","catalog":"3"},
    "00775B": {"name":"新光投等債15+","catalog":"4"},
    "00783": {"name":"富邦中証500","catalog":"2"},
    "008201": {"name":"BP上證50","catalog":"2"},
    "00830": {"name":"國泰費城半導體","catalog":"4"},
    "00850": {"name":"元大臺灣ESG永續","catalog":"1"},
    "00851": {"name":"台新全球AI","catalog":"6"},
    "00852L": {"name":"國泰美國道瓊正2","catalog":"4"},
    "00861": {"name":"元大全球未來通訊","catalog":"6"},
    "00865B": {"name":"國泰US短期公債","catalog":"4"},
    "00866": {"name":"新光Shiller CAPE","catalog":"4"},
    "00875": {"name":"國泰網路資安","catalog":"6"},
    "00876": {"name":"元大未來關鍵科技","catalog":"6"},
    "00878": {"name":"國泰永續高股息","catalog":"1"},
}

async def getText(parentNode, css):
    return parentNode.find(css).text()

async def getResults(parentNode, css):
    return parentNode.find(css).items()

def updatAllStock(result, data):
    if not bool(result) and bool(data):
        # for init state when server is just activated, latest_reuslt = {}
        result = data
    else:
        for stockid in data.keys():
            for itemName in data[stockid]:
                value = data[stockid][itemName]

                if len(value) > 0:
                    result[stockid][itemName] = value

    return result

async def connect(pool:ProxyPool, url:str, useProxy:bool=True):
    if useProxy:
        connector = pool.connectorGen()
    else: 
        connector = None

    try:
        async with ClientSession(connector=connector) as session:
            async with session.get(
                url, 
                headers=await ProxyPool.headGen(), 
                timeout=setting.req_timeout ) as resp:

                html = await resp.text()
    
    except Exception as e:
        utillogger.debug(f"{e}")
        html = None

    return html