import logging

class MyLogger:
    def __init__(self, name:str, level:str="debug"):
        self.name = name

        assert level in ["debug", "info", "error"], f"Unknows logging level in {self.name}-Logger"
        self.logLevel = level
        
        self.debug_fmt = logging.Formatter('[%(asctime)s][%(funcName)s]: %(message)s')
        self.info_fmt = logging.Formatter('[%(asctime)s]: %(message)s')

    def createLogger(self):
        newlogger = logging.getLogger(self.name)
        handler = logging.StreamHandler()

        if self.logLevel == "debug":
            newlogger.setLevel(logging.DEBUG)
            handler.setFormatter(self.debug_fmt)

        elif self.logLevel == "info":
            newlogger.setLevel(logging.INFO)
            handler.setFormatter(self.info_fmt)

        elif self.logLevel == "error":
            newlogger.setLevel(logging.ERROR)

        else:
            assert False, f"Unknows logging level in {self.name} Logger"

        newlogger.addHandler(handler)

        return newlogger

def test_mylogger():
    mylogger = MyLogger("an", "info").createLogger()
    mylogger.info("this is info")
    
    mylogger.setLevel(logging.ERROR)
    mylogger.info("this is info2")
    mylogger.debug("this is debug2")

mainlogger = MyLogger("main", "info").createLogger()
utillogger = MyLogger("util", "info").createLogger()
proxylogger = MyLogger("proxy-pool", "info").createLogger()
twselogger = MyLogger("twse-handler", "info").createLogger()
twstklogger = MyLogger("twstk-handler", "info").createLogger()