from utils.mylogger import proxylogger
import asyncio
import random
import os
from pathlib import Path

from datetime import datetime
from typing import Union
from aiohttp import ClientSession, ClientTimeout
from aiohttp_socks import ProxyConnector
from pyquery import PyQuery as pq
from typing import Tuple

from fake_useragent import UserAgent
ua = UserAgent()


class ProxyPool():
    def __init__(self):
        self.__proxyList = []

    @property
    def proxyList(self):
        return self.__proxyList

    @staticmethod
    async def headGen() -> dict:
        """
        產生用於偽裝的標頭
        """
        RefererList = [
            "https://www.learncodewithmike.com/",
            "https://www.google.com/",
            "https://www.baidu.com/",
            "https://www.msn.com/zh-tw/",
            "https://www.hnfhc.com.tw/HNFHC/?language=en/",
            "https://www.jamicon.com.tw/"
        ]

        return {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7",
            "Referer": random.choice(RefererList),
            "User-Agent": ua.random
        }

    @staticmethod
    def removeDuplicateProxy(proxies):
        return list(set(proxies))

    def connectorGen(self):
        proxy = random.choice(self.__proxyList)
        proxylogger.debug(f"==== generate proxy:{proxy} ====")

        connector = ProxyConnector.from_url(f'socks4://{proxy}')
        return connector

    async def _getRawProxyFromWeb(self) -> list:
        """
        從 https://www.socks-proxy.net/ 爬取 socks4-proxy
        """
        async with ClientSession() as session:
            async with session.get("https://www.socks-proxy.net/") as response:
                return pq(await response.text())("textarea").text().split("\n")[3:-1]

    async def _getProxyFromFile(self):

        proxyPath = Path.cwd() / "proxy"
        assert proxyPath.exists(), "proxy file is not exist ..."

        with open(proxyPath, "r") as fd:
            context = fd.read().strip().split("\n")

            if len(context) > 1:
                return context
            else:
                return []

    async def _verify(self, socksProxy: str, timeout: int = 10) -> Union[str, None]:
        """
        @socksProxy: 網路上獲取的 socks4-proxy
        @timeout: 用於驗證proxy的可用性時，等待 proxy 反應的時間，
        timeout 的時間越長，有可能會得到反應慢的 proxy，但可以得到較多的 proxy，
        若 proxy 的數量太少時，可以適量增加，預設值為10
        """
        proxylogger.debug(f"verify proxy: {socksProxy} ...")

        connector = ProxyConnector.from_url(f'socks4://{socksProxy}')
        timeout = ClientTimeout(total=timeout)

        async with ClientSession(connector=connector) as session:
            try:
                async with session.get(
                    "https://www.twse.com.tw",
                        headers=await ProxyPool.headGen(), timeout=timeout) as response:

                    proxylogger.debug(f"{socksProxy} ... {response.status}")

                    return socksProxy

            except Exception as e:
                proxylogger.debug(f"{socksProxy} ... fail")
                return

    async def get(self, timeout: int = 10) -> list:
        """
        從 
            1_proxy.txt 中取得曾經驗證過的 proxy，但有可能失效的，
            2_網路獲取即時的 proxy，但可能有重複的，
        將兩者都重新驗證過後，返回最新有效的 proxy
        """
        rawProxyList = await self._getRawProxyFromWeb()
        rawProxyList.extend(await self._getProxyFromFile())

        # 移除重複
        rawProxyList = ProxyPool.removeDuplicateProxy(rawProxyList)
        # 驗證
        tasks = [asyncio.create_task(self._verify(
            socksProxy, timeout=timeout)) for socksProxy in rawProxyList]

        validList = await asyncio.gather(*tasks, return_exceptions=True)

        # to avoid error result in validList
        return [proxy for proxy in validList if isinstance(proxy, str)]

    async def _add2ProxyFile(self, validList: list, limit: int = 300):
        '''
        將 proxy 寫入檔案中
        '''
        proxylogger.debug(
            f"==== adding {len(validList)} latest-proxy into file")

        proxyPath = Path.cwd() / "proxy"
        assert proxyPath.exists(), "proxy file is not exist ..."

        with open(proxyPath, "w") as fw:
            tmpList = [item+"\n" for item in validList[:limit]]
            #readList[-1] = readList[-1].strip()

            fw.writelines(tmpList)

    async def updatePool4ever(self, interval: int = 30, timeout: int = 10, amount: int = 300) -> list:
        """
        @interval: proxy-pool 更新的頻率，預設值為 30s
        @timeout: 判斷 proxy 是否有效的超時時間，預設值為 10s
        @amount: 限制寫進 proxy-file 的最大數量，預設值為 300個
        """
        while True:
            now = str(datetime.now())

            proxylogger.info(f"[BG]: update proxy-pool @ {now}")

            # get validated proxy from file and web
            self.__proxyList = await self.get(timeout=timeout)
            await self._add2ProxyFile(self.__proxyList, limit=amount)

            await asyncio.sleep(interval)

    async def singleUpdate(self, timeout: int = 10, amount: int = 300) -> list:
        """
        @timeout: 判斷 proxy 是否有效的超時時間，預設值為 10s
        @amount: 限制寫進 proxy-file 的最大數量，預設值為 300個
        更新機制見updatePool4ever() 的說明
        """
        now = str(datetime.now())

        # get validated proxy from file and web
        self.__proxyList = await self.get(timeout=timeout)
        await self._add2ProxyFile(self.__proxyList, limit=amount)

        return self.__proxyList


async def check(p):
    while True:
        proxylogger.debug(
            f"==== checking latest proxyList \n{len(p.proxyList)}\n{p.proxyList}")


async def run():
    p = ProxyPool()
    task1 = asyncio.create_task(p.updatePool4ever(interval=30, timeout=20))
    task2 = asyncio.create_task(check(p))

    await asyncio.gather(task1, task2, return_exceptions=True)


def run_example():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())
