aiohttp==3.7.3
aiohttp-socks==0.5.5
fake-useragent==0.1.11
pyquery==1.4.3
twstock==1.3.1
gunicorn==20.0.4
uvloop==0.14.0