
import asyncio
import twstock
import json
import time
import random
import sys
import traceback
from datetime import datetime
from aiohttp import ClientSession, ClientTimeout

from utils.proxypool import ProxyPool
from utils.mylogger import twstklogger
from utils.util import connect

from settings import setting


class Twstk:
    def __init__(self, proxypool: ProxyPool):
        self._data = {}
        self.req_timeout = setting.req_timeout
        self._proxypool = proxypool

    @property
    def data(self):
        return self._data

    async def _getSingleMonth(self, stockid: str):
        """
        _getSingleMonth()，獲得單一股票的每月交易資訊
        """
        today = datetime.now().strftime("%Y%m%d")

        datas = {stockid: {
            "本月每日日期": [],
            "本月每日收盤價": [],
            "本月每日最高價": [],
            "本月每日最低價": [],
        }}

        try:
            html = await connect(
                pool=self._proxypool,
                url=f"https://www.twse.com.tw/exchangeReport/STOCK_DAY?response=json&date={today}&stockNo={stockid}",
                useProxy=setting.twstk.useProxy
            )

            if html == None:
                return datas

            raws = json.loads(html)["data"]
            for row in raws:
                datas[stockid]["本月每日日期"].append(row[0])
                datas[stockid]["本月每日收盤價"].append(row[6])
                datas[stockid]["本月每日最高價"].append(row[4])
                datas[stockid]["本月每日最低價"].append(row[5])

        except Exception as e:
            twstklogger.debug(f"{e}")

        finally:
            return datas

    async def _getMonthDatas(self, stockNames: list):
        """
        _getMonthDatas()，獲得多個股票的每月交易資訊
        """
        datas = {}

        tasks = [asyncio.create_task(self._getSingleMonth(
            stockName)) for stockName in stockNames]
        results = await asyncio.gather(*tasks)

        for stock in results:
            stockid = list(stock.keys())[0]

            datas[stockid] = {
                "本月每日日期": stock[stockid]["本月每日日期"],
                "本月每日收盤價": stock[stockid]["本月每日收盤價"],
                "本月每日最高價": stock[stockid]["本月每日最高價"],
                "本月每日最低價": stock[stockid]["本月每日最低價"],
            }

        return datas

    async def _getAllStock(self, stockNames: list):
        """
        _getAllStock()，獲得多個股票的每日即時交易資訊
        """
        results = {}

        for name in stockNames:
            results[name] = {}

        query_str = twstock.realtime._join_stock_id(stockNames)
        query_time = int(time.time()) * 1000

        try:
            html = await connect(
                pool=self._proxypool,
                url=f"https://mis.twse.com.tw/stock/api/getStockInfo.jsp?ex_ch={query_str}&_={query_time}",
                useProxy=setting.twstk.useProxy
            )

            if html == None:
                return results

            raws = json.loads(html)["msgArray"]

            for row in raws:
                stockid = row["c"]

                results[stockid] = {
                    "股票名": row["n"],
                    "本月每日日期": [],
                    "本月每日收盤價": [],
                    "本月每日最高價": [],
                    "本月每日最低價": [],
                    "最新成交價格": row["z"],
                    "當日交易量": row["tv"],
                    "累積成交量": row["v"],
                    "當日開盤價": row["o"],
                    "當日最高價": row["h"],
                    "當日最低價": row["l"],
                    "realtime-update":  datetime.fromtimestamp(int(row["tlong"]) / 1000).strftime('%Y-%m-%d %H:%M:%S')
                }

        except Exception as e:
            # 取得錯誤類型
            error_class = e.__class__.__name__
            # 取得詳細內容
            detail = e.args[0]

            twstklogger.debug(f"{error_class}，{detail}")

        finally:
            return results

    async def _handler(self, stockNames):
        realtimeTask = asyncio.create_task(self._getAllStock(stockNames))
        monthdataTask = asyncio.create_task(self._getMonthDatas(stockNames))

        realtimeResult = await realtimeTask
        monthdataResult = await monthdataTask

        # ==== 合併 realtimeResult 和 monthdataResult 的結果 ====
        for sid in monthdataResult.keys():
            realtimeResult[sid]["本月每日日期"] = monthdataResult[sid]["本月每日日期"]
            realtimeResult[sid]["本月每日收盤價"] = monthdataResult[sid]["本月每日收盤價"]
            realtimeResult[sid]["本月每日最高價"] = monthdataResult[sid]["本月每日最高價"]
            realtimeResult[sid]["本月每日最低價"] = monthdataResult[sid]["本月每日最低價"]

        return realtimeResult

    async def run(self):
        self._data = await self._handler(setting.twstk.targetStocks)
        return "twstk done"


def test_twstk():
    twstk = Twstk()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(twstk.run())
    print(json.dumps(twstk.data, ensure_ascii=False))
