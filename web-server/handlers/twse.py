import asyncio
import json
from aiohttp import ClientSession, ClientTimeout
from pyquery import PyQuery as pq

from utils.proxypool import ProxyPool
from utils.myselector import CSS as css
from utils.util import getResults, getText, nameMap, catalogMap, connect
from utils.mylogger import twselogger
from settings import setting


class Twse:
    def __init__(self, proxypool: ProxyPool):
        self._data = {}
        self._proxypool = proxypool

    @property
    def data(self):
        return self._data

    @staticmethod
    def getNameCatalog(identify, name):
        if identify in nameMap:
            mapName = nameMap[identify]["name"]
            catalog = catalogMap[nameMap[identify]["catalog"]]
        else:
            mapName = name
            catalog = catalogMap["7"]

        return catalog, mapName

    @staticmethod
    def normalize(s):
        if "," in str(s):
            s = s.replace(",", "")

        return float(s)

    async def _handler(self, html):
        result = {}

        # 在 result 中建立分類
        for cat in catalogMap.values():
            result[cat] = []

        # 解析資料
        data = json.loads(html)
        for msg in data["a1"]:

            # 過濾空集合
            if "msgArray" in msg.keys():

                # 過濾系統錯誤時，msgArray 為空
                if isinstance(msg["msgArray"], list):

                    datas = msg["msgArray"]

                    for index, data in enumerate(msg["msgArray"]):
                        catalog, mapName = Twse.getNameCatalog(
                            data["a"], data["b"])

                        row = {
                            "ETF代號/ETF名稱": "{}/{}".format(data["a"], mapName),
                            "已發行受益權單位數": "{:,}".format(Twse.normalize(data["c"])),
                            "與前日已發行受益單位差異數": "{}".format(data["d"]),
                            "成交價": "{}".format(data["e"]),
                            "投信或總代理人預估淨值": "{}".format(data["f"]),
                            "預估折溢價幅度": "{}".format(data["g"]),
                            "前一營業日單位淨值": "{}".format(data["h"]),
                            "預估折溢價幅度": "{}".format(data["g"]),
                            "更新日期時間": "{} {}".format(data["i"], data["j"]),
                            "預估折溢價幅度": "{}".format(data["g"]),
                        }

                        result[catalog].append(row)

        return result

    async def run(self):
        html = await connect(pool=self._proxypool, url=css.url.twse, useProxy=setting.twse.useProxy)
        doc = pq(html)
        self._data = await self._handler(doc.text())

        return "twse done"
