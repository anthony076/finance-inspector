import asyncio
from aiohttp import ClientSession
from pyquery import PyQuery as pq

from utils.myselector import CSS as css
from utils.util import getResults, getText


class Stockq:
    def __init__(self):
        self._data = {}

    @property
    def data(self):
        return self._data

    @staticmethod
    def _nameReplace(s):
        special_name = ["Binance Coin", "Bitcoin SV",
                        "USD Coin", "S&P 500", "中國 A50"]

        for name in special_name:
            if name in s:
                s = s.replace(name, name.replace(" ", "-"))
        return s

    async def _handler(self, table):
        result = {}

        tableName = await getText(table, css.stockq.catalogName)
        result[tableName] = []

        values = table(css.stockq.values).text()
        values = Stockq._nameReplace(values).split(" ")

        for i in range(0, len(values)-4, 5):
            name, index, updown, ratio, update = values[i], values[i+1], values[i+2], values[i+3], values[i+4]

            result[tableName].append({
                "name": name,
                "index": index,
                "updown": updown,
                "ratio": ratio,
                "update": update
            })

        return result

    async def run(self):
        async with ClientSession() as session:
            async with session.get(css.url.stockq) as response:

                html = await response.text()
                doc = pq(html)
                tables = doc(css.stockq.tables).items()

                tasks = [asyncio.create_task(self._handler(table))
                         for table in tables]
                datas = await asyncio.gather(*tasks, return_exceptions=True)

                for data in datas:
                    key = list(data.keys())[0]
                    self.data[key] = data[key]

        return "stockq done"
